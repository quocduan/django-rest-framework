from django.contrib.auth.models import User, Group
from rest_framework import serializers



# class GroupSerializer(serializers.HyperlinkedModelSerializer):
class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        # fields = ('url', 'name')
        fields = ('id', 'name')

# class UserSerializer(serializers.HyperlinkedModelSerializer):
class UserSerializer(serializers.ModelSerializer):
    groups=GroupSerializer(many=True, read_only=True)
    class Meta:
        model = User
        # fields = ('url', 'username', 'email', 'groups')
        fields = ('id', 'username', 'email', 'groups')


